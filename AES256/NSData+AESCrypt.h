//
//  NSData+AESCrypt.h
//  AES256
//
//  Created by Minal Soni on 15/04/18.
//  Copyright © 2018 Minal Soni. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (AESCrypt)
- (NSData *)AES256EncryptWithKey:(NSString *)key;
- (NSData *)AES256DecryptWithKey:(NSString *)key;
- (NSString *)base64Encoding;
+ (NSData *)dataWithBase64EncodedString:(NSString *)string;
@end
