//
//  main.m
//  AES256
//
//  Created by Minal Soni on 15/04/18.
//  Copyright © 2018 Minal Soni. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
