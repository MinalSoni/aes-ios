//
//  AppDelegate.h
//  AES256
//
//  Created by Minal Soni on 15/04/18.
//  Copyright © 2018 Minal Soni. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

