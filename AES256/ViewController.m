//
//  ViewController.m
//  AES256
//
//  Created by Minal Soni on 15/04/18.
//  Copyright © 2018 Minal Soni. All rights reserved.
//

#import "ViewController.h"
#import "NSString+AESCrypt.h"
@interface ViewController ()
@property(nonatomic,weak) IBOutlet UITextView *key;
@property(nonatomic,weak) IBOutlet UITextView *phrase;
@property(nonatomic,weak) IBOutlet UITextView *result;
@end

@implementation ViewController
-(IBAction)encrypt:(id)sender{
    if(_key.text.length > 0 && _phrase.text.length > 0){
        [_result setText:[_phrase.text AES256EncryptWithKey:_key.text]];
    }else{
        [_result setText:@"Invalid input"];
    }
}

-(IBAction)decrypt:(id)sender{
    if(_key.text.length > 0 && _phrase.text.length > 0){
        [_result setText:[_phrase.text AES256DecryptWithKey:_key.text]];
    }else{
        [_result setText:@"Invalid input"];
    }
}
@end
