//
//  NSString+AESCrypt.h
//  AES256
//
//  Created by Minal Soni on 15/04/18.
//  Copyright © 2018 Minal Soni. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (AESCrypt)
- (NSString *)AES256EncryptWithKey:(NSString *)key;
- (NSString *)AES256DecryptWithKey:(NSString *)key;
@end
